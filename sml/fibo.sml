
fun f(n : int,a:int,b:int)=
    if n=0
    then a
    else if n=1
    then b
    else f(n-1,b,a+b)

fun flist(n :int, result:int list)=
    if n<0
    then result
    else flist(n-1,f(n,0,1)::result)
		    
fun sumlist(x: int list, sum:int)=
    if null x
    then sum
    else sumlist(tl x,(hd x) + sum)
		       
(* still there are overflow exceptions above 45, possibly int or whatever*)

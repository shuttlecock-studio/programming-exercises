
fun is_older(a : int*int*int, b : int*int*int)=
    if #1 a < #1 b
    then true
    else if #1 a = #1 b andalso #2 a < #2 b
    then true
    else if #1 a = #1 b andalso #2 a = #2 b andalso #3 a < #3 b
    then true
    else false;

fun number_in_month(dates:(int*int*int) list,month:int)=
    let	
	fun filterish(list :(int*int*int) list,total: int)=
	    if null list
	    then total
	    else if #2(hd list)= month
	    then filterish(tl list,total+1)
	    else filterish(tl list, total)				   
    in
	filterish(dates,0)
    end;

fun number_in_months(dates:(int*int*int)list,months: int list)=
    let
	fun loop_months(list:int list, total: int)=
	    if null list
	    then total
	    else loop_months(tl list, total + number_in_month(dates,hd list))
    in
	loop_months(months,0)
    end;


fun dates_in_month(dates:(int*int*int)list,month:int)=
    let
	fun filterish(list:(int*int*int)list, result: (int*int*int)list)=
	    if null list
	    then result
	    else if #2(hd list)= month
	    then filterish(tl list,hd list::result)
	    else filterish(tl list, result);

	fun reverse(list:(int*int*int)list)=
	    let
		fun lreverse(list:(int*int*int)list,rlist:(int*int*int)list)=
		    if null list
		    then rlist
		    else lreverse(tl list, hd list::rlist);
	    in
		lreverse(list,[])
	    end;
    in
	reverse(filterish(dates,[]))
    end;

fun dates_in_months(dates:(int*int*int)list,months:int list)=
    let
	fun loop_months(list:int list,result:(int*int*int)list)=
	    if null list
	    then result
	    else loop_months(tl list,result @ dates_in_month(dates,hd list))
    in
	loop_months(months,[])
    end;



fun get_nth(list:string list, n:int)=
    if n=1
    then hd list
    else get_nth(tl list,n-1);

fun date_to_string(date:(int*int*int))=
    let
	val monthlist=["January","February","March","April","May",
		       "June","July","August","September","October",
		       "November","December"]
    in
	get_nth(monthlist,#2 date)^" "^Int.toString(#3 date)^", "^Int.toString(#1 date)
    end;


fun number_before_reaching_sum(sum:int,list:int list)=
    let
	fun realfun(list: int list,position:int,subtotal:int)=
	    if (subtotal+ hd list)>=sum
	    then position-1
	    else realfun(tl list, position+1,subtotal+hd list)
    in
	realfun(list,1,0)
    end;

fun what_month(day:int)=
    let
	val monthdays=[31,28,31,
		   30,31,30,
		   31,31,30,
		   31,30,31]
    in
	number_before_reaching_sum(day,monthdays)+1
    end;
							
			  
fun month_range(day1:int,day2:int)=
    let
	fun reverse(list:int list)=
	    let
		fun lreverse(list:int list,rlist:int list)=
		    if null list
		    then rlist
		    else lreverse(tl list, hd list::rlist);
	    in
		lreverse(list,[])
	    end;
	
	fun expand(m:int,n:int,result:int list)=
	    if m = n
	    then reverse(what_month(n)::result)
	    else expand(m+1,n,what_month(m)::result);
    in
	if day1>day2
	then []
	else expand(day1, day2,[])
    end;

fun oldest(dates:(int*int*int)list)=
    let
	fun find_oldest(list:(int*int*int)list, oldest:(int*int*int))=
	    if null list
	    then oldest
	    else if is_older(hd list, oldest)
	    then find_oldest(tl list,hd list)
	    else find_oldest(tl list, oldest);
    in
	if null dates
	then NONE
	else SOME (find_oldest(dates,hd dates))
    end;

fun number_in_months_challenge(dates:(int*int*int)list,months: int list)=
    let
	fun reverse(list:int list)=
	    let
		fun lreverse(list:int list,rlist:int list)=
		    if null list
		    then rlist
		    else lreverse(tl list, hd list::rlist);
	    in
		lreverse(list,[])
	    end;
	fun deduplicate(list:int list)=
	    let
		fun checklist(elem:int,list:int list)=
		    if null list
		    then true
		    else if elem = hd list
		    then false
		    else checklist(elem, tl list);
		
		fun ldeduplicate(list:int list,result:int list)=
		    if null list
		    then reverse(result)
		    else if checklist(hd list,result)
		    then ldeduplicate(tl list, hd list::result)
		    else ldeduplicate(tl list, result);
	    in
		ldeduplicate(list,[])
	    end;
    in
	number_in_months(dates,deduplicate(months))
    end;


fun dates_in_months_challenge(dates:(int*int*int)list,months:int list)=
    let
	fun reverse(list:int list)=
	    let
		fun lreverse(list:int list,rlist:int list)=
		    if null list
		    then rlist
		    else lreverse(tl list, hd list::rlist);
	    in
		lreverse(list,[])
	    end;
	fun deduplicate(list:int list)=
	    let
		fun checklist(elem:int,list:int list)=
		    if null list
		    then true
		    else if elem = hd list
		    then false
		    else checklist(elem, tl list);
		
		fun ldeduplicate(list:int list,result:int list)=
		    if null list
		    then reverse(result)
		    else if checklist(hd list,result)
		    then ldeduplicate(tl list, hd list::result)
		    else ldeduplicate(tl list, result);
	    in
		ldeduplicate(list,[])
	    end;
    in
	dates_in_months(dates,deduplicate(months))
    end;

fun reasonable_date(date:(int*int*int))=
    let
	val year=[31,28,31,
		   30,31,30,
		   31,31,30,
		   31,30,31];
	val leapyear=[31,29,31,
		   30,31,30,
		   31,31,30,
		   31,30,31];
	fun isleapyear(year:int)=
	    year mod 400 = 0 orelse (year mod 4 =0 andalso year mod 100<>0);
	fun get_nth_int(list:int list, n:int)=
	    if n=1
	    then hd list
	    else get_nth_int(tl list,n-1);

					
    in
	if #1 date > 0
	then if #2 date >= 1 andalso #2 date <=12
	     then if isleapyear(#1 date)
		  then if #3 date>0 andalso
			  #3 date <= get_nth_int(leapyear,#2 date)
		       then true
		       else false
		  else if #3 date>0 andalso
			  #3 date <= get_nth_int(year,#2 date)
		  then true
		  else false
	     else false
	else false
    end;

				
			       



	       
	       
	       
	       



	       

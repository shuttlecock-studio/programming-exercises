(* Dan Grossman, Coursera PL, HW2 Provided Code *)

(* if you use this function to compare two strings (returns true if the same
   string), then you avoid several of the functions in problem 1 having
   polymorphic types that may be confusing *)
fun same_string(s1 : string, s2 : string) =
    s1 = s2;

fun reverse(list)=
    let
	fun lreverse(list, result)=
	    case list of
		[]=>result
	      | x::xs' =>lreverse(xs',x::result)
    in
	lreverse(list,[])
    end;




(* put your solutions for problem 1 here *)

fun all_except_option( str, list)=
    let
	fun lall_except_option(list,collect)=
	    case list of
		[]=>NONE
	      | x::xs' => if same_string(str, x)
			  then SOME (reverse(collect)@xs')
			  else lall_except_option(xs',x::collect);
    in
	lall_except_option(list,[])
    end;

fun get_substitutions1(list , str)=(*string list list*)
    case list of
	[]=>[]
      | x::xs' =>case all_except_option(str, x) of
		     NONE=>get_substitutions1(xs',str)
		   | SOME y => y@get_substitutions1(xs',str);

fun get_substitutions2(list , str)=(*string list list*)
    let
	fun lget_substitutions2(list,result)=
	    case list of
		[]=>result
	      | x::xs' =>case all_except_option(str, x) of
			     NONE=>lget_substitutions2(xs',result)
			   | SOME y => lget_substitutions2(xs',result@y);
    in
	lget_substitutions2(list,[])
    end;
fun similar_names(list,fullname)=
    let
	val {first=a,middle=b,last=c}=fullname;
	fun iterate(a_list)=
	    case a_list of
		[]=>[]
	      | x::xs' =>{first=x,middle=b,last=c}::iterate(xs');
    in
	fullname::iterate(get_substitutions2(list,a))
    end;

(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int 
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw 

exception IllegalMove;


(* put your solutions for problem 2 here *)

fun card_color(card)=
    let
	val (suit,rank)=card;
    in
	case suit of
	    Clubs=>Black
	  | Diamonds =>Red
	  | Hearts =>Red
	  | Spades =>Black
    end;

fun card_value(card)=
    let
	val(suit,rank)=card;
    in
	case rank of
	    Num x=>x
	  | Ace =>11
	  | _ =>10
    end;

fun remove_card(cs,c,e)=
    let
	fun lremove_card(list,collect)=
	    case list of
		[]=>raise e
	      | x::xs' => if x=c
			  then reverse(collect)@xs'
			  else lremove_card(xs',x::collect)
    in
	lremove_card(cs,[])
    end;




fun all_same_color(cs)=
    case cs of
	x::xs::xs' => (card_color x = card_color xs)
		      andalso all_same_color(xs::xs')
      | _ =>true;

fun sum_cards(cs)=
    let
	fun lsum_cards(cs,acc)=
	    case cs of
		[]=>acc
	      | x::xs' =>lsum_cards(xs',acc+card_value x)
    in
	lsum_cards(cs,0)
    end;
fun score(cs,goal)=
    let
	val sum = sum_cards(cs);
	val prelim_score= if sum > goal
			  then 3*(sum-goal)
			  else goal-sum;
    in
	if all_same_color cs
	then prelim_score div 2
	else prelim_score
    end;

fun officiate(cardlist,movelist,goal)=
    let
	fun lofficiate(cardlist,movelist,heldlist)=
	    (case movelist of
		 []=>score(heldlist,goal)
	       | Discard c::cs'=>
		 lofficiate(cardlist,cs',remove_card(heldlist,c,IllegalMove))
	       | Draw::ys'=>
		 case cardlist of
		     []=>score(heldlist,goal)
		   | x::xs' => if sum_cards(x::heldlist)>goal
			       then score(x::heldlist,goal)
			       else lofficiate(xs',ys',x::heldlist))
    in
	lofficiate(cardlist,movelist,[])

    end;



(* sum_cards_challenge should give a list*)

fun sum_cards_challenge(list)=
    let
	fun mapadd(list,num)=
	    case list of
		[]=>[]
	      | x::xs' => (x+num)::mapadd(xs',num);
	fun lsum_cards_challenge(list,result)=
	    case list of
		[]=>result
	      | y::ys'=>
		let
		    val (suit,rank)=y;
		    val modified_result=
			case rank of
			    Num a=>mapadd(result,a)
			  | Ace =>mapadd(result,1)@mapadd(result,11)
			  | _ =>mapadd(result,10);
		in
		    lsum_cards_challenge(ys',modified_result)
		end
    in
	lsum_cards_challenge(list,[0])
    end;


fun score_challenge(cs,goal)=
    let
	fun scoresfromsums(sumlist)=
	    case sumlist of
		[]=>[]
	      | x::xs' =>
		let
		    val prelim_score= if x >goal
				      then 3*(x-goal)
				      else goal-x;
		    val score= if all_same_color cs
			       then prelim_score div 2
			       else prelim_score;
		in
		    score::scoresfromsums xs'
		end;
	fun bestscore(scorelist,result)=
	    case scorelist of
		[]=>result
	      | x::xs' => if x < result
			  then bestscore(xs',x)
			  else bestscore(xs',result);
    in
	bestscore(scoresfromsums(sum_cards_challenge(cs)),10000)
    end;

fun officiate_challenge(cardlist,movelist,goal)=
    let
	fun allgreater(list,num)=
	    case list of
		[]=>true
	      | x::xs' => x>num andalso allgreater(xs',num);
	
	fun lofficiate_challenge(cardlist,movelist,heldlist)=
	    (case movelist of
		 []=>score_challenge(heldlist,goal)
	       | Discard c::cs'=>
		 lofficiate_challenge(cardlist,cs',remove_card(heldlist,c,IllegalMove))
	       | Draw::ys'=>
		 case cardlist of
		     []=>score_challenge(heldlist,goal)
		   | x::xs' => if allgreater(sum_cards_challenge(x::heldlist),goal)
			       then score_challenge(x::heldlist,goal)
			       else lofficiate_challenge(xs',ys',x::heldlist))
    in
	lofficiate_challenge(cardlist,movelist,[])

    end;


fun careful_player(cardlist,goal)=
    let
	fun discard_and_draw(list,topcard,original_list)=
	    case list of
		[]=>NONE
	      | x::xs' =>
		if score(topcard::(remove_card(original_list,x,IllegalMove)),goal)=0
		then SOME x
		else discard_and_draw(xs',topcard,original_list);
	
	fun lcareful_player(cardlist,heldlist,movelist)=
	    if score(heldlist,goal)=0
	    then movelist
	    else case cardlist of
		     []=> if goal -sum_cards(heldlist) >10
			  then Draw::movelist
			  else movelist

		   | x::xs' =>case discard_and_draw(heldlist,x,heldlist)of
				  NONE =>  if goal -sum_cards(heldlist) >10
					   then lcareful_player(xs',
							  x::heldlist,
							  movelist@[Draw])
					   else movelist
				| SOME y => lcareful_player(xs',
							    x::remove_card(heldlist,y,IllegalMove),
							    movelist@[Discard y,Draw]) 
    in
	lcareful_player(cardlist,[],[])
    end;
	
			




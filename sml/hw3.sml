(* Coursera Programming Languages, Homework 3, Provided Code *)

exception NoAnswer

datatype pattern = Wildcard
		 | Variable of string
		 | UnitP
		 | ConstP of int
		 | TupleP of pattern list
		 | ConstructorP of string * pattern

datatype valu = Const of int
	      | Unit
	      | Tuple of valu list
	      | Constructor of string * valu

fun g f1 f2 p =
    let 
	val r = g f1 f2 
    in
	case p of
	    Wildcard          => f1 ()
	  | Variable x        => f2 x
	  | TupleP ps         => List.foldl (fn (p,i) => (r p) + i) 0 ps
	  | ConstructorP(_,p) => r p
	  | _                 => 0
    end

(**** for the challenge problem only ****)

datatype typ = Anything
	     | UnitT
	     | IntT
	     | TupleT of typ list
	     | Datatype of string;
(**** you can put all your code here ****)

fun only_capitals(list)=
    List.filter (fn e =>Char.isUpper(String.sub(e,0)))  list;


fun longest_string1(list)=
    case list of
	[]=>""
      | _ => (List.foldl (fn (e,acc) => if String.size e > String.size acc
					then e
					else acc) "" list);


fun longest_string2(list)=
     case list of
	[]=>""
      | _ => (List.foldl (fn (e,acc) => if String.size e >= String.size acc
					then e
					else acc) "" list);


fun longest_string_helper f list =
    case list of
	[]=>""
      | _ =>(List.foldl (fn (e, acc)=>if f(String.size e,String.size acc)
				      then e
				      else acc) "" list);


val longest_string3 = longest_string_helper (fn (x,y)=> x>y);

val longest_string4 = longest_string_helper (fn (x,y)=> x>=y);


val longest_capitalized = longest_string1 o only_capitals;

fun rev_string s= String.implode (List.rev (String.explode s));

fun first_answer f list =
    case list of
	[]=>raise NoAnswer
      | x::xs' =>case f x of
		     NONE=> first_answer f xs'
		   | SOME v =>v;


fun all_answers f list=
    let
	fun lall_answers f list collect=
	    case list of
		[]=> SOME collect
	      | x::xs' =>case f x of
			     NONE=>NONE
			   | SOME v =>lall_answers f xs' (collect@v);
    in
	lall_answers f list []
    end;


val count_wildcards = g (fn a=>1) (fn a=>0);

val count_wild_and_variable_lengths= g (fn a=>1) (fn a=> String.size a);

fun count_some_var (s,p)=
    g (fn a=>0) (fn a=>if a = s then 1 else 0) p;

fun check_pat p=
    let
	fun getvars p=
	    case p of
		Variable x =>[x]
	      | TupleP ps => List.foldl (fn (e,acc) =>acc@(getvars e)) [] ps
	      | ConstructorP (_,p) =>getvars p
	      | _ =>[];
	fun checkvars xs =
	    case xs of
		[]=>true
	      | x::xs' =>(not(List.exists (fn e=>e=x) xs'))andalso checkvars xs';
    in
	checkvars (getvars p)
    end;


fun match (v,p)=
    case p of
	Wildcard=>SOME []
      | Variable s =>SOME [(s,v)]
      | UnitP =>( case v of
		      Unit =>SOME []
		    | _ => NONE)
      | ConstP x =>(case v of
		       Const y=> if x =y then SOME [] else NONE
		    | _ => NONE)
      | ConstructorP(s1,pat) =>(case v of
				Constructor(s2,vlu)=>if s1=s2
						   then match (vlu,pat)
						   else NONE
			      | _ =>NONE)
      | TupleP ps =>
	(let
	    fun tuplematch list=
		    if List.length list = List.length ps
		    then all_answers match (ListPair.zip(list,ps))
		    else NONE;
	in
	    case v of
		Tuple []=>tuplematch []
	      | Tuple (x::xs') =>tuplematch (x::xs')
	      | _=>NONE
	end);

fun first_match v ps=
    let
	fun gen_tuple arg list=
	    case list of
		[]=>[]
	      | x::xs' => (arg,x)::(gen_tuple arg xs');
    in
	SOME (first_answer(match) (gen_tuple v ps))
	handle NoAnswer =>NONE
    end;

(*fun typecheck_patterns (cs, ps)=
    case cs of
	*)
    
	
			      
		
    

	    

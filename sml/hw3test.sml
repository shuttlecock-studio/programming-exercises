(* Homework3 Simple Test*)
(* These are basic test cases. Passing these tests does not guarantee that your code will pass the actual homework grader *)
(* To run the test, add a new line to the top of this file: use "homeworkname.sml"; *)
(* All the tests should evaluate to true. For example, the REPL should say: val test1 = true : bool *)

val test1 = only_capitals ["A","B","C"] = ["A","B","C"];

val testa = only_capitals ["Al","Bo","Ca","al","bo"] = ["Al","Bo","Ca"];


val test2 = longest_string1 ["A","bc","C"] = "bc";

val testb = longest_string1 ["A","cat","dog","bc","C"] = "cat";


val test3 = longest_string2 ["A","bc","C"] = "bc";

val testc = longest_string2 ["A","cat","dog","bc","C"] ="dog";

val test4a = longest_string3 ["A","bc","C"] = "bc";

val testd1 = longest_string3 ["A","cat","dog","bc","C"] = "cat";



val test4b = longest_string4 ["A","B","C"] = "C";

val testd2 = longest_string4 ["A","BB","CC"] = "CC";


val test5 = longest_capitalized ["A","bc","C"] = "A"

val test6 = rev_string "abc" = "cba"

val test7 = first_answer (fn x => if x > 3 then SOME x else NONE) [1,2,3,4,5] = 4

val test8 = all_answers (fn x => if x = 1 then SOME [x] else NONE) [2,3,4,5,6,7] = NONE;
val test8a = all_answers (fn x => if x > 1 then SOME [x] else NONE) [2,3,4,5,6,7] = SOME [2,3,4,5,6,7];

val test8b = all_answers (fn x => if x > 1 then SOME [x] else NONE) [] = SOME [];

val test8c = all_answers (fn x => if x = 1 then SOME [x] else NONE) [1,2,3,4,5,6,7] = NONE;

val pattern1 = Wildcard;

val pattern2= Variable "abc";

val pattern3 = UnitP;

val pattern4= ConstP 3;

val pattern5= TupleP[Wildcard, Variable "def", UnitP, ConstP 7];

val pattern6= ConstructorP("constructor",TupleP[UnitP,ConstP 9, Variable "ghi",Wildcard]);

val pattern7=TupleP[pattern6,Wildcard];

val pattern8= ConstructorP("ghi",pattern7);

val pattern9=TupleP[pattern5,pattern2,pattern7,pattern8];


val test9a = count_wildcards Wildcard = 1;

val test9= count_wildcards pattern7=2;

val test9b = count_wild_and_variable_lengths (Variable("a")) = 1;

val test9e = count_wild_and_variable_lengths pattern7 = 5;


val test9c = count_some_var ("x", Variable("x")) = 1;


val test9f = count_some_var ("ghi", pattern7) = 1;

val test9g = count_some_var ("ghi", pattern8) = 1;



val test10 = check_pat (Variable("x")) = true;
val test10a= check_pat pattern9=false;
val test10b= check_pat pattern8=true;
val test10c=check_pat (TupleP[pattern5,pattern8,pattern2])=true;



val test11 = match (Const(1), UnitP) = NONE;

val test11a = match (Const(1), ConstP(17))=NONE;

val test11b = match (Const(17), ConstP(17))=SOME[];

val test11c = match (Const 17,Variable "x")=SOME[("x",Const 17)];

val test11c = match (Tuple [Const 17,Constructor("abc",(Tuple[Unit,Const 5]))],Variable "y")=SOME[("y",Tuple [Const 17,Constructor("abc",(Tuple[Unit,Const 5]))])];
val test11d= match(Unit,UnitP)=SOME[];

val test11e= match(Tuple[],UnitP)=NONE;
val test11f= match(Tuple[Const 5,Const 7, Const 8, Const 9],TupleP [Variable "a", Variable "b",Variable "c", Variable "d"])=SOME [("a",Const 5),("b",Const 7),("c",Const 8),("d",Const 9)];

val test11g= match(Tuple[Const 5,Const 7, Const 8, Const 9],TupleP [Variable "a", Variable "b",Variable "c", UnitP])=NONE;

val test11h= match(Constructor("test",Tuple[Const 5,Const 7, Const 8, Const 9]),ConstructorP("test",TupleP [Variable "a", Variable "b",Variable "c", Variable "d"]))= SOME [("a",Const 5),("b",Const 7),("c",Const 8),("d",Const 9)];

val test12 = first_match Unit [UnitP] = SOME []
